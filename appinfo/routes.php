<?php
namespace OCA\Simple_Files\Appinfo;

$application = new Application();

/** @var $this \OC\Route\Router */

$this->create('simple_files_index', '/')
	->actionInclude('simple_files/index.php');
