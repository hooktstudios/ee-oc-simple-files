<?php
/**
 * Copyright (c) 2014 Lukas Reschke <lukas@owncloud.com>
 * This file is licensed under the Affero General Public License version 3 or
 * later.
 * See the COPYING-README file.
 */

namespace OCA\Simple_Files\Appinfo;

class Application extends \OCA\Files\Appinfo\Application {
	public function __construct(array $urlParams=array()) {
		// HMM? Name is no longer what we'd like, but should still work
		parent::__construct($urlParams);
	}
}
