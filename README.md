# Simple Files

File listing without sidebar and breadcrumb, with a GET-param injected title
(used to link to a ID folder while still providing context for
users).
